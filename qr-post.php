<?php
  /*
   Plugin Name: QR Post
   Plugin URI: http://www.qrblog.nl
   Description: Replace you post text with a QR code image containing your post.
   Version: 0.1
   Author: Emiel Kremers
   Author URI: http://www.qrblog.nl
   */
  
  $qrpost_dir = dirname(__FILE__) . '/';
  $qrpost_cache_dir = dirname(__FILE__) . '/cache/';
  
  /**
   * qr_code
   *
   * generate qr code
   * based on QRcode image PHP Scripts from Y.Swetake
   * @access  public
   * @param   string  $data data
   * @param   string  $ecc  ECC level (L or M or Q or H)
   * @param   string  $type J:jpeg image , other: PNG image
   * @param   integer $size dafault PNG:4 JPEG:8
   * @param   integer $version 1-40 or Auto select if you do not set
   * @return  string
   */
  function qr_code($data, $ecc = 'M', $type = 'J', $size = '8', $version = null)
  {
      global $qrpost_cache_dir, $qrpost_dir;
      $params = array('d' => $data, 'e' => $ecc, 't' => $type, 's' => $size, 'v' => $version, );
      $cache_id = md5(serialize($params));
      $cache_file = $cache_id . ($type == 'J' ? '.jpeg' : '.png');
      if (is_writable($qrpost_cache_dir) && !is_readable($qrpost_cache_dir . $cache_file)) {
          //$qrcode_data_string = urlencode($data);
          $qrcode_data_string = $data;
          $qrcode_error_correct = $ecc;
          $qrcode_module_size = $size;
          $qrcode_version = $version;
          $qrcode_image_type = $type;          

          ob_start();
          require_once($qrpost_dir . 'php/qr_img.php');
          $out = ob_get_contents();
          ob_end_clean();
          
          $cache = fopen($qrpost_cache_dir . $cache_file, 'w+');
          fwrite($cache, $out);
          fclose($cache);
      } elseif (!is_writable($qrpost_cache_dir)) {
          return get_option('siteurl') . '/wp-content/plugins/qr-post/image/cache_not_found.jpg';
      }
      return get_option('siteurl') . '/wp-content/plugins/qr-post/cache/' . $cache_file;
  }
 
  function add_qrpost($content)
  {
	if(!is_page()) {
         $format = get_option('qrpost_format');
         $size = get_option('qrpost_size');
         $ecc = get_option('qrpost_ecc');
      
         $myContent = str_replace(" ", "+", get_the_content());
         $myContent = html_entity_decode($myContent);
      
         $image = '<img src="' . qr_code(get_the_content(), $ecc, $format, $size) . '"/> ';
         $div = '<div style="display: none;">' . get_the_content() . '</div>';
         return $image . $div;
      } else {
         return $content;
      }
  }
 
  function qrpost_option()
  {

$format = get_option('qrpost_format');
$size = get_option('qrpost_size');
$ecc = get_option('qrpost_ecc');

?>
<div class="wrap">
<h2>QR Post</h2>
<form method="post" action="options.php">
<?php
      wp_nonce_field('update-options');
?>
<table class="form-table"

<tr valign="top">
<th scope="row">ECC</th>
<td>
<input class="radio" id="qrpost_ecc" name="qrpost_ecc" type="radio"  value="L" <?php if ($ecc == "L") { echo ' checked="checked"'; }?>>L 7% of codewords can be restored.<br>
<input class="radio" id="qrpost_ecc" name="qrpost_ecc" type="radio"  value="M" <?php if ($ecc == "M") { echo ' checked="checked"'; }?>>M 15% of codewords can be restored.<br>
<input class="radio" id="qrpost_ecc" name="qrpost_ecc" type="radio"  value="Q" <?php if ($ecc == "Q") { echo ' checked="checked"'; }?>>Q 25% of codewords can be restored. <br>
<input class="radio" id="qrpost_ecc" name="qrpost_ecc" type="radio"  value="H" <?php if ($ecc == "H") { echo ' checked="checked"'; }?>>H 30% of codewords can be restored.
</td>
</tr>

<tr valign="top">
<th scope="row">Format</th>
<td>
<input class="radio" id="qrpost_format" name="qrpost_format" type="radio"  value="J" <?php if ($format == "J") { echo '  checked="checked"'; }?>>Jpeg
<input class="radio" id="qrpost_format" name="qrpost_format"   type="radio" value="P"  <?php if ($format == "P") { echo ' checked="checked"'; }?>>Png
</td>
</tr>

<tr valign="top">
<th scope="row">Size</th>
<td><input type="text" name="qrpost_size" value="<?php echo $size; ?>" /></td>
</tr>

</table>

<input type="hidden" name="action" value="update" />
<input type="hidden" name="page_options" value="qrpost_size, qrpost_format, qrpost_ecc" />
<p class="submit">
<input type="submit" class="button-primary" value="<?php
      _e('Save Changes')
?>" />
</p>
</form>
</div>

<?php
  }
  
  function qrpost_install()
  {
      $qrpost_cache_dir = dirname(__FILE__) . '/cache/';
      add_option('qrpost_format', "J");
      add_option('qrpost_size', "8");
      add_option('qrpost_ecc', "M");
      error_log($qrpost_cache_dir, 0);
      if (!is_dir($qrpost_cache_dir)) {
          mkdir($qrpost_cache_dir);
      }
  }
  
  function qrpost_add_admin()
  {
      add_options_page('QR Post', 'QR Post', 7, 'qrpost', 'qrpost_option');
  }
  
  function qrpost_uninstall()
  {
      delete_option('qrpost_format');
      delete_option('qrpost_size');
      delete_option('qrpost_ecc');
  }
  
  add_action('admin_menu', 'qrpost_add_admin');
  add_filter('the_content', 'add_qrpost');
  register_activation_hook(__FILE__, "qrpost_install");
  register_deactivation_hook(__FILE__, 'qrpost_uninstall');
?>